use flume::Sender;
use futures::stream::StreamExt;
use reqwest_eventsource::{Event, EventSource};

use crate::{errors::MobTimeError, events::MobEvent, message::MobTimeMessage};

pub(crate) async fn handle(es: &mut EventSource, tx: Sender<MobTimeMessage>) {
    while let Some(event) = es.next().await {
        match event {
            Ok(Event::Open) => {
                // Initialization of the Connection is ignored.
            }
            Ok(Event::Message(_)) => {
                let event = MobEvent::from_stream_event(&event.unwrap());
                match event {
                    Ok(event_type) => match event_type {
                        MobEvent::InitialHistory(_) => {
                            // let msg = MobTimeMessage::Timer(crate::events::Data::default());
                            // tx.send(msg).unwrap();
                        }
                        MobEvent::TimerRequest(timer) => {
                            let msg = MobTimeMessage::Timer(timer);
                            tx.send(msg).unwrap();
                        }
                        MobEvent::KeepAlive => {
                            // We ignore handle KeepAlive Events
                        }
                    },
                    Err(e) => {
                        let msg = MobTimeMessage::Error(e);
                        tx.send(msg).unwrap();
                    }
                };
            }
            Err(err) => {
                tx.send(MobTimeMessage::Error(MobTimeError::EventSource(err)))
                    .unwrap();
                es.close();
            }
        }
    }
}
