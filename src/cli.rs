use std::error::Error;
use std::fmt;
use std::str::FromStr;

use clap::Parser;

use serde::{Deserialize, Serialize};

#[derive(Parser, Default, Debug, Clone, Serialize, Deserialize)]
#[clap(author, about, version, name = "mobtime", long_about = None)]
pub(crate) struct CliArgs {
    /// The user of the mob timer
    #[clap(
        long,
        value_parser,
        env = "MOB_TIMER_USER",
        overrides_with = "mob_timer_user"
    )]
    pub mob_timer_user: Option<String>,
    /// The name of the mob timer room
    #[clap(
        long,
        value_parser,
        env = "MOB_TIMER_ROOM",
        overrides_with = "mob_timer_room"
    )]
    pub mob_timer_room: Option<String>,
    /// The url of the mob timer
    #[clap(
        long,
        value_parser,
        env = "MOB_TIMER_URL",
        overrides_with = "mob_timer_url",
        default_value = "https://timer.mob.sh/"
    )]
    pub mob_timer_url: String,
    /// Value in minutes of no changes
    #[clap(long, value_parser, overrides_with = "timeout")]
    pub timeout: Option<usize>,
    /// Allow counting longer than a turn.
    #[clap(long, value_parser, overrides_with = "overshoot")]
    pub overshoot: bool,
    /// Interval of timer output
    #[clap(long, value_parser, overrides_with = "interval")]
    pub interval: Option<usize>,
    /// Output format
    #[clap(long, overrides_with = "format")]
    pub format: Option<Format>,
}

impl CliArgs {
    pub(crate) fn mob_timer_room(&self) -> Option<&String> {
        self.mob_timer_room.as_ref()
    }

    pub(crate) fn try_timer_room(&self) -> Result<String, CliError> {
        if let Some(room) = self.mob_timer_room() {
            Ok(room.into())
        } else {
            Err(CliError(
                "Could not find a MOB_TIMER_ROOM, please specify one.".into(),
            ))
        }
    }

    pub(crate) fn mob_timer_url(&self) -> &str {
        self.mob_timer_url.as_ref()
    }
}

#[derive(Clone, Default, Debug, Serialize, Deserialize)]
pub(crate) enum Format {
    Json,
    #[default]
    Default,
}

impl FromStr for Format {
    type Err = CliError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "json" | "Json" => Ok(Format::Json),
            "default" | "Default" => Ok(Format::Default),
            _ => Err(CliError(
                "Format takes one of the following arguments: json, default.".into(),
            )),
        }
    }
}

#[derive(Debug, Clone)]
pub struct CliError(std::string::String);

impl CliError {
    fn new(msg: &str) -> Self {
        Self(msg.to_string())
    }
}

impl fmt::Display for CliError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl Error for CliError {
    fn description(&self) -> &str {
        &self.0
    }
}
