use thiserror::Error;

use crate::cli::CliError;

#[derive(Error, Debug)]
pub(crate) enum MobTimeError {
    // Deserialization error
    #[error("Deserialization error: {0}")]
    Serde(#[from] serde_json::Error),
    // Io error
    #[error("IoError: {0}")]
    Io(#[from] std::io::Error),
    // Io error with path context
    // #[error("IoError: {0}, File: {1}")]
    // IoPath(std::io::Error, std::path::PathBuf),
    // Internal Deserialization Error
    #[error("FromUtf8Error: {0}")]
    FromUtf8(#[from] std::string::FromUtf8Error),
    // Reqwest Eventource Error
    #[error("EventSourceError: {0}")]
    EventSource(#[from] reqwest_eventsource::Error),
    // MobTimeError
    #[error("MobTimeError: {0}")]
    MobTimeError(std::string::String),
    // MobCliError
    #[error("MobCliError: {0}")]
    MobCliError(#[from] CliError),
}
