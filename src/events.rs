use chrono::DateTime;
use chrono::Utc;
use reqwest_eventsource::Event;
use serde::{Deserialize, Serialize};

use crate::errors::MobTimeError;

#[derive(Debug, Serialize, Deserialize)]
pub(crate) enum Events {
    #[serde(rename = "INITIAL_HISTORY")]
    InitialHistory,
    #[serde(rename = "BREAK")]
    Break,
    #[serde(rename = "TIMER_REQUEST")]
    TimerRequest,
}

#[derive(Debug, Serialize, Deserialize)]
pub(crate) struct Data {
    timer: Option<i64>,
    // #[serde(with = "ts_seconds_option")]
    requested: Option<DateTime<Utc>>,
    user: Option<String>,
    #[serde(rename = "nextUser")]
    next_user: Option<String>,
    #[serde(rename = "type")]
    data_type: Option<String>,
}

impl Data {
    fn from_str(str: &str) -> Result<Self, MobTimeError> {
        let data: Self = serde_json::from_str(str)?;
        Ok(data)
    }
    fn from_vec(str: &str) -> Result<Vec<Self>, MobTimeError> {
        let data: Vec<Self> = serde_json::from_str(str)?;
        Ok(data)
    }

    pub(crate) fn timer(&self) -> Option<i64> {
        self.timer
    }

    pub(crate) fn requested(&self) -> Option<DateTime<Utc>> {
        self.requested
    }

    pub(crate) fn user(&self) -> Option<&String> {
        self.user.as_ref()
    }

    pub(crate) fn next_user(&self) -> Option<&String> {
        self.next_user.as_ref()
    }

    pub(crate) fn data_type(&self) -> Option<&String> {
        self.data_type.as_ref()
    }
}

#[derive(Debug)]
pub(crate) enum MobEvent {
    /// Initial sync of the History
    InitialHistory(Vec<Data>),
    /// Timer and Break timer Requests
    TimerRequest(Data),
    /// Keep alive Event
    KeepAlive,
}

impl MobEvent {
    pub(crate) fn from_stream_event(event: &Event) -> Result<Self, MobTimeError> {
        if let Event::Message(event) = event {
            match event.event.as_str() {
                "INITIAL_HISTORY" => {
                    return Ok(MobEvent::InitialHistory(Data::from_vec(&event.data)?))
                }
                "TIMER_REQUEST" => return Ok(MobEvent::TimerRequest(Data::from_str(&event.data)?)),
                "KEEP_ALIVE" => return Ok(MobEvent::KeepAlive),
                _ => {
                    println!("Unhandled Event {}", event.event);
                    return Ok(MobEvent::KeepAlive);
                }
            }
        }
        Err(MobTimeError::MobTimeError("This is an Open Event.".into()))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn data_deserializes() {
        let data = r#"{"timer":10,"requested":"2022-09-15T07:22:45.829231Z","user":"a-kenji","nextUser":"nuno","type":"BREAKTIMER"}"#;

        Data::from_str(data).unwrap();
    }
}
