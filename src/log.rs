use tracing::Level;
use tracing_subscriber::{EnvFilter, FmtSubscriber};

pub const LOG_ENV: &str = "MOBTIME_LOG";

/// Configuration of logging
/// # Panics:
/// Panics, if the subscriber can't set the global default.
pub fn init_logging() {
    let subscriber = FmtSubscriber::builder()
        // all spans/events with a level higher than TRACE (e.g, debug, info, warn, etc.)
        // will be written to output path.
        .with_max_level(Level::TRACE)
        .with_thread_ids(true)
        .with_ansi(true)
        .with_line_number(true);

    if let Ok(env_filter) = EnvFilter::try_from_env(LOG_ENV) {
        let subscriber = subscriber.with_env_filter(env_filter).finish();
        tracing::subscriber::set_global_default(subscriber)
            .expect("setting default subscriber failed");
    } else {
        let subscriber = subscriber.finish();
        tracing::subscriber::set_global_default(subscriber)
            .expect("setting default subscriber failed");
    }
}
