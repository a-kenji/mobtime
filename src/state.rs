use std::convert::TryFrom;

use chrono::{DateTime, Utc};
use flume::Receiver;
use serde::{Deserialize, Serialize};

use crate::{cli::CliArgs, errors::MobTimeError, message::MobTimeMessage};

pub(crate) struct State {
    receiver: Receiver<MobTimeMessage>,
    timer: Option<Timer>,
    config: CliArgs,
}

struct Timer {
    user: String,
    requested: DateTime<Utc>,
    is_break: bool,
    timer: i64,
}

#[derive(Default, Serialize, Deserialize, Debug)]
pub(crate) struct MobTimerOutput {
    user: String,
    time: String,
    is_break: bool,
}

impl MobTimerOutput {
    fn display(&self) {
        println!("{:?}", self);
    }
    fn pretty_print(&self) {
        if self.is_break() {
            println!(" ☕ Breaktime:  {} ", self.time());
        } else {
            println!("  {} : {} ", self.user(), self.time());
        }
    }
    fn display_json(self) -> Result<(), MobTimeError> {
        let deserialized = serde_json::to_string(&self)?;
        println!("{}", deserialized);
        Ok(())
    }

    pub(crate) fn is_break(&self) -> bool {
        self.is_break
    }

    pub(crate) fn time(&self) -> &str {
        self.time.as_ref()
    }

    pub(crate) fn user(&self) -> &str {
        self.user.as_ref()
    }
}

impl Timer {
    fn new(user: String, requested: DateTime<Utc>, is_break: bool, timer: i64) -> Self {
        Self {
            user,
            requested,
            is_break,
            timer,
        }
    }

    fn user(&self) -> &str {
        self.user.as_ref()
    }

    fn requested(&self) -> DateTime<Utc> {
        self.requested
    }

    fn timer(&self) -> i64 {
        self.timer
    }

    fn is_break(&self) -> bool {
        self.is_break
    }
}

impl TryFrom<MobTimeMessage> for Timer {
    type Error = MobTimeError;

    fn try_from(message: MobTimeMessage) -> Result<Self, Self::Error> {
        match message {
            MobTimeMessage::Timer(data) => Ok(Timer::new(
                data.user().unwrap().to_string(),
                data.requested().unwrap(),
                data.data_type() != Some(&"TIMER".into()),
                data.timer().unwrap(),
            )),
            MobTimeMessage::Error(e) => Err(e),
        }
    }
}

impl State {
    pub(crate) fn new(receiver: Receiver<MobTimeMessage>, config: CliArgs) -> Self {
        Self {
            config,
            receiver,
            timer: None,
        }
    }

    fn set_timer(&mut self, timer: Timer) {
        self.timer = Some(timer);
    }

    fn to_output(&self) -> MobTimerOutput {
        if let Some(timer) = &self.timer {
            let duration = chrono::Duration::minutes(timer.timer());
            let time = timer
                .requested()
                .time()
                .signed_duration_since(Utc::now().time())
                .checked_add(&duration)
                .unwrap();

            let time = if !time.num_minutes().is_negative() && time.num_seconds().is_negative() {
                format!(
                    "{:02}:{:02}",
                    time.num_minutes(),
                    time.num_seconds().abs() % 60
                )
            } else if time.num_seconds().is_negative() {
                format!("00:00")
            } else if !time.num_minutes().is_negative() {
                format!(
                    "{:02}:{:02}",
                    time.num_minutes(),
                    time.num_seconds().abs() % 60
                )
            } else {
                format!(
                    "{:02}:{:02}",
                    time.num_minutes(),
                    time.num_seconds().abs() % 60
                )
            };

            MobTimerOutput {
                user: timer.user().to_string(),
                time,
                is_break: timer.is_break(),
            }
        } else {
            MobTimerOutput::default()
        }
    }

    pub(crate) async fn main(&mut self) -> Result<(), MobTimeError> {
        let mut last_update = Utc::now();
        loop {
            if let Ok(msg) = self.receiver.try_recv() {
                self.set_timer(msg.try_into()?);
                last_update = Utc::now();
            }

            self.to_output().pretty_print();

            std::thread::sleep(std::time::Duration::new(1, 0));

            let since_last_update = Utc::now()
                .time()
                .signed_duration_since(last_update.time())
                .num_minutes();

            if since_last_update > 20 {
                println!("❄");
                std::process::exit(0);
            }
        }
    }
}
