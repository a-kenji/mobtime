use crate::{errors::MobTimeError, events::Data};

#[derive(Debug)]
pub(crate) enum MobTimeMessage {
    Timer(Data),
    Error(MobTimeError),
}
