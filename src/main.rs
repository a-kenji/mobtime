use crate::cli::CliArgs;
use clap::Parser;
use reqwest_eventsource::EventSource;

use self::{errors::MobTimeError, state::State};
type MobTimeResult = Result<(), MobTimeError>;

mod cli;
mod errors;
mod events;
mod handler;
mod log;
mod message;
mod state;

#[tokio::main]
async fn main() -> MobTimeResult {
    // log::init_logging();
    let opts = CliArgs::parse();

    let (tx, rx) = flume::unbounded();

    let room = opts.try_timer_room()?;
    let url = opts.mob_timer_url();
    let room_url = format!("{url}{room}/events");
    tracing::debug!("{room_url}");
    let mut es = EventSource::get(room_url);

    tokio::spawn(async move {
        handler::handle(&mut es, tx).await;
    });

    State::new(rx, opts).main().await?;

    Ok(())
}
