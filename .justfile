alias r := run
alias rr := run-release
alias f := fmt
alias l := lint
alias d := doc
alias b := build
alias uf := update-flake-dependencies
alias uc := update-cargo-dependencies

_default:
  @just --choose


run:
    cargo run 

fmt:
    cargo fmt
    treefmt --config-file .treefmt.toml --tree-root ./.

build:
    cargo build

lint:
    cargo clippy
    nix run nixpkgs#typos

doc:
    cargo doc --open --offline --document-private-items

run-release:
    cargo run --release

update-flake-dependencies:
    nix flake update --commit-lock-file

# Update and then commit the `Cargo.lock` file
update-cargo-dependencies:
    cargo update
    git add Cargo.lock
    git commit Cargo.lock -m "update(cargo): \`Cargo.lock\`"
