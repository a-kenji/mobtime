# mobtime
<p align="center">
  <a href="https://builtwithnix.org"><img alt="Built with nix" src="https://img.shields.io/static/v1?label=built%20with&message=nix&color=5277C3&logo=nixos&style=flat-square&logoColor=ffffff"></a>
  <a href="https://crates.io/crates/mobtime"><img alt="Mobtime Version Information" src="https://img.shields.io/crates/v/flk?style=flat-square"</a>
</p>

Cli for speaking with a mob timer room.
