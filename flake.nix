{
  description = "Cli for querying information from a mob timer room.";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    rust-overlay.url = "github:oxalica/rust-overlay";
    rust-overlay.inputs.nixpkgs.follows = "nixpkgs";
    rust-overlay.inputs.flake-utils.follows = "flake-utils";
    flake-compat.url = "github:edolstra/flake-compat";
    flake-compat.flake = false;
    crane.url = "github:ipetkov/crane";
  };

  outputs = {
    self,
    nixpkgs,
    rust-overlay,
    flake-utils,
    crane,
    flake-compat,
  }:
    flake-utils.lib.eachSystem
    [
      "aarch64-linux"
      "aarch64-darwin"
      "i686-linux"
      "x86_64-darwin"
      "x86_64-linux"
    ]
    (
      system: let
        overlays = [(import rust-overlay)];

        pkgs = import nixpkgs {inherit system overlays;};

        inherit (cargoToml.package) name version;
        root = self;

        stdenv =
          if pkgs.stdenv.isLinux
          then pkgs.stdenvAdapters.useMoldLinker pkgs.stdenv
          else pkgs.stdenv;

        ignoreSource = [
          ".git"
          "target"
          "example"
        ];

        src = pkgs.nix-gitignore.gitignoreSource ignoreSource root;

        cargoToml = builtins.fromTOML (builtins.readFile (src + "/Cargo.toml"));
        rustToolchainToml = pkgs.rust-bin.fromRustupToolchainFile (
          src + "/rust-toolchain.toml"
        );

        cargoLock = {
          lockFile = builtins.path {
            path = src + "/Cargo.lock";
            name = "Cargo.lock";
          };
        };
        cargo = rustToolchainToml;
        rustc = rustToolchainToml;

        buildInputs = [
          pkgs.openssl
          pkgs.installShellFiles
        ];
        nativeBuildInputs = [pkgs.pkg-config];
        devInputs = [
          rustToolchainToml

          pkgs.rust-analyzer

          pkgs.just

          pkgs.cargo-flamegraph
        ];
        fmtInputs = [
          pkgs.alejandra
          pkgs.treefmt
        ];
        ciInputs = [
          pkgs.typos
          pkgs.reuse
          pkgs.cargo-deny
        ];
        editorConfigInputs = [pkgs.editorconfig-checker];

        # crane
        args = {
          inherit src buildInputs nativeBuildInputs;
        };
        buildCrane =
          ((crane.mkLib pkgs).overrideToolchain rustToolchainToml).buildPackage;
        buildCraneArtifacts =
          ((crane.mkLib pkgs).overrideToolchain rustToolchainToml).buildDepsOnly
          args;

        ASSET_DIR = "./target/assets";
        targetDir = "target/${
          pkgs.rust.toRustTarget pkgs.stdenv.targetPlatform
        }/release";
        # assetDir = "${name}/${targetDir}/assets";
        # assetDir = "${name}/${ASSET_DIR}";
        assetDir = "target/assets";
        postInstall = ''
            # install the manpage
            installManPage ${assetDir}/${name}.1
          # explicit behavior
          cp ${assetDir}/${name}.bash ./completions.bash
          installShellCompletion --bash --name ${name}.bash ./completions.bash
          cp ${assetDir}/${name}.fish ./completions.fish
          installShellCompletion --fish --name ${name}.fish ./completions.fish
          cp ${assetDir}/_${name} ./completions.zsh
          installShellCompletion --zsh --name _${name} ./completions.zsh
        '';

        meta = with pkgs.lib; {
          homepage = "https://github.com/a-kenji/mobtime/";
          description = "Cli for speaking with the mob timer room";
          license = [licenses.mit];
        };
      in rec {
        packages.default =
          (pkgs.makeRustPlatform {inherit cargo rustc;}).buildRustPackage
          {
            inherit
              src
              name
              version
              stdenv
              cargoLock
              buildInputs
              nativeBuildInputs
              ASSET_DIR
              postInstall
              meta
              ;
          };
        packages.crane = buildCrane (
          args
          // {
            cargoDepsName = name;
            # GIT_DATE = gitDate;
            # GIT_REV = gitRev;
            doCheck = false;
            ASSET_DIR = "${targetDir}/assets/";
            src = pkgs.lib.cleanSourceWith {
              src = (crane.mkLib pkgs).path ./.; # The original, unfiltered source
            };
            inherit
              name
              version
              stdenv
              # src
              
              # nativeBuildInputs
              
              # buildInputs
              
              # cargoLock
              
              # postInstall
              
              ;
          }
        );
        # nix run
        apps.default = flake-utils.lib.mkApp {drv = packages.default;};

        devShells = {
          default = (pkgs.mkShell.override {inherit stdenv;}) {
            inherit buildInputs name ASSET_DIR;
            nativeBuildInputs = nativeBuildInputs ++ devInputs ++ fmtInputs ++ ciInputs;
            RUST_BACKTRACE = 1;
          };
          fmtShell = pkgs.mkShell {
            name = "fmt";
            nativeBuildInputs = fmtInputs;
          };
          ciShell = pkgs.mkShell {
            name = "ci";
            nativeBuildInputs = ciInputs;
          };
          editorConfigShell = pkgs.mkShell {
            name = "editor-config";
            nativeBuildInputs = editorConfigInputs;
          };
        };

        formatter = pkgs.alejandra;
      }
    )
    // {
      overlays = {
        default = _: prev: {inherit (self.packages.${prev.system}) mobtime;};
        nightly = _: prev: {inherit (self.packages.${prev.system}) mobtime;};
      };
    };
}
